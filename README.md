### What is this repository for? ###
For fun


* Just a simple Blog on Go

# How to use:

* download go https://golang.org/dl/
* cd to_the_repo
* go get
* go build
* ./go-blog

# for mongo db, use Vagrant file
### You will need:

  * Git 1.7+
  * Vagrant v1.6.5 + (http://vagrantup.com)
  * Virtualbox v4.3.16 + (https://www.virtualbox.org/)
# info
  * mongodb will be running at localhost:27017