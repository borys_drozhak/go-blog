package main

import (
	"bitbucket.org/borys_drozhak/go-blog/db/documents"
	"bitbucket.org/borys_drozhak/go-blog/sessions"
	"bitbucket.org/borys_drozhak/go-blog/utils"
	// "fmt"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"html/template"
	"labix.org/v2/mgo"
	"net/http"
	"time"
)

const (
	COOKIE_NAME = "sessionId"
	PORT        = ":8080"
)

type Post struct {
	Id, Title       string
	ContentMarkdown string
	ContentHtml     string
}

// var posts map[string]Post
var postsCollections *mgo.Collection
var inMemorySession *session.Session

func main() {

	inMemorySession = session.NewSession()
	// posts = make(map[string]Post, 0) // for posts
	sessions, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	postsCollections = sessions.DB("blog").C("posts")

	m := martini.Classic()

	unescapeFunMap := template.FuncMap{"unescape": unescape} // trick for markdown -> html in templates

	m.Use(render.Renderer(render.Options{
		Directory:  "templates",                        // Specify what path to load the templates from.
		Layout:     "layout",                           // Specify a layout template. Layouts can call {{ yield }} to render the current template.
		Extensions: []string{".tmpl", ".html"},         // Specify extensions to load for templates.
		Funcs:      []template.FuncMap{unescapeFunMap}, // Specify helper fun map for access templates
		Charset:    "UTF-8",                            // Sets encoding for json and html content-types. Default is "UTF-8".
		IndentJSON: true,                               // Output human readable JSON
		IndentXML:  true,                               // Output human readable XML
	}))

	staticOptions := martini.StaticOptions{Prefix: "assets"}
	m.Use(martini.Static("assets", staticOptions))

	m.Get("/", indexHandler)
	m.Get("/write", writeHandler)
	m.Get("/login", getLoginHandler)
	m.Post("/login", postLoginHandler)
	m.Post("/SavePost", savePostHandler)
	m.Get("/edit/:id", editPostHandler)
	m.Get("/delete/:id", deletePostHandler)
	m.Post("/gethtml", getHtmlHandler)

	m.RunOnAddr(PORT)
}

func unescape(x string) interface{} {
	return template.HTML(x)
}

type error interface {
	Error() string
}

// errorString is a trivial implementation of error.
type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

// New returns an error that formats as the given text.
func NewError(text string) error {
	return &errorString{text}
}

func checkUserSession(req *http.Request, c string) (user string, err error) {
	cookie, _ := req.Cookie(c)
	if cookie != nil {
		user = inMemorySession.Get(cookie.Value)
	} else {
		// New(errorString)
		err = NewError("session: no user session")
	}
	return
}

func indexHandler(r render.Render, req *http.Request) {

	_, err := checkUserSession(req, COOKIE_NAME)
	if err != nil {
		r.HTML(401, "login", "please log in")
		return
	}

	postDocuments := []documents.PostDocument{}
	postsCollections.Find(nil).All(&postDocuments)

	post := Post{}
	posts := []Post{}

	for _, doc := range postDocuments {
		post = Post{doc.Id, doc.Title, doc.ContentMarkdown, doc.ContentHtml}
		posts = append(posts, post)
	}

	r.HTML(200, "index", posts)
}

func writeHandler(r render.Render, req *http.Request) {
	user, err := checkUserSession(req, COOKIE_NAME)
	if err != nil {
		r.HTML(401, "login", "please log in")
		return
	}
	signuter := "\n\nadded by " + user + "."
	post := Post{
		ContentMarkdown: signuter,
		ContentHtml:     signuter,
	}
	r.HTML(200, "write", post)
}

func getLoginHandler(r render.Render) {
	r.HTML(200, "login", nil)
}

func postLoginHandler(r render.Render, req *http.Request, w http.ResponseWriter) {
	password := req.FormValue("password")
	username := req.FormValue("username")
	if username == "" || password == "" {
		r.HTML(401, "login", "error: Something wrong")
	} else {

		print("POSTLOGIN ACTION!", password, username)

		sessionId := inMemorySession.Init(username)
		cookie := &http.Cookie{
			Name:    COOKIE_NAME,
			Value:   sessionId,
			Expires: time.Now().Add(5 * time.Minute),
		}

		http.SetCookie(w, cookie)
		r.Redirect("/")
	}
}

func savePostHandler(r render.Render, req *http.Request) {

	id := req.FormValue("id")
	title := req.FormValue("title")
	contentMarkdown := req.FormValue("content")
	contentHtml := utils.ConvertMarkdownToHtml(contentMarkdown)

	postDocument := documents.PostDocument{id, title, contentMarkdown, contentHtml}

	if id != "" {
		// posts[id] = Post{id, title, contentHtml, contentMarkdown}
		postsCollections.UpdateId(id, postDocument)
	} else {
		postDocument.Id = utils.GenerateId() // collisions should be handled
		postsCollections.Insert(postDocument)
		// posts[id] = Post{id, title, contentHtml, contentMarkdown}
	}

	r.Redirect("/")

}

func editPostHandler(r render.Render, p martini.Params, req *http.Request) {

	_, err := checkUserSession(req, COOKIE_NAME)
	if err != nil {
		r.HTML(401, "login", "please log in")
		return
	}

	id := p["id"]
	// post, found := posts[id]
	// if !found {
	postDocument := documents.PostDocument{}
	err = postsCollections.FindId(id).One(&postDocument)
	if err != nil {
		r.Redirect("/")
		return
	}
	post := Post{postDocument.Id, postDocument.Title, postDocument.ContentMarkdown, postDocument.ContentHtml}

	r.HTML(200, "write", post)
}

func deletePostHandler(r render.Render, p martini.Params) {
	id := p["id"]
	if id != "" {
		// delete(posts, id)
		postsCollections.RemoveId(id)
	}

	r.Redirect("/")
}

func getHtmlHandler(r render.Render, req *http.Request) {
	md := req.FormValue("md")
	// htmlBytes := blackfriday.MarkdownBasic([]byte(md))
	html := utils.ConvertMarkdownToHtml(md)
	r.JSON(200, map[string]interface{}{"html": html})
}
