sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

sudo apt-get update

sudo apt-get install -y mongodb-org


# Listen to local interface only. Comment out to listen on all interfaces.
echo 'set up mongo for listen on all interfaces'
sudo sed -i 's/bindIp: 127.0.0.1/#&/' /etc/mongod.conf

sudo service mongod restart